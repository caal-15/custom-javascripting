# Corriendo el Servidor de Markdown

Instala los requerimientos usando `npm install`.

Para ver los retos accediendo desde tu navegador a `http://localhost:3000`, simplemente corre el comando `npm start`.

# Introducción

Estos son una serie de retos basados en el workshop __javascripting__, con el propósito de entrenar personas nuevas en el lenguaje, y como parte de un curso dirigido en el __Nodeschool Day__.

# Retos

1. [Calentando](1_warming_up)
1. [Strings](2_strings)
1. [Numbers](3_numbers)
1. [Control](4_control_statements)
1. [Funciones](5_functions)
1. [Arreglos](6_arrays)
1. [Objetos](7_objects)

__NOTA:__ La solución a cada uno de los retos está en su respectiva carpeta.