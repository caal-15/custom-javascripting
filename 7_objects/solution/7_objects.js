const gallardo = {
  color: 'yellow',
  maxSpeed: 320,
  amenities: {
    seats: 'leather',
    others: ['Bluetooth Player', 'High Accuracy GPS']
  }
}

const printObject = (obj, level=0) => {
  let prepend = ''
  for (let i = 0; i < level; i++) {
    prepend += '  '
  }
  for (key in obj) {
    if (typeof(obj[key]) === 'object' && !Array.isArray(obj[key])) {
      console.log(prepend + key + ':')
      printObject(obj[key], level + 1)
    } else {
      console.log(prepend + key + ': ' + obj[key])
    }
  }
}

printObject(gallardo)