let oranges = 'oranges'
const originalLength = oranges.length

for (let i = 0; i < 6; i++) {
  oranges += '!'
  const dif = oranges.length - originalLength
  if (dif < 3) {
    console.log('You are not an orange lover')
  } else if (dif >= 3 && dif <=5) {
    console.log('You like oranges')
  } else {
    console.log('You love oranges!')
  }
}