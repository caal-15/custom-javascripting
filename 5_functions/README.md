# El Reto

Crea una función que tome tres argumentos, tome los primeros dos y los multiplique entre si y los concatene el tercer argumento (un `String`) con el resultado, retorna al cadenada formada. Luego, llama la función con los siguiente argumentos: `4`, `6`, `'I want this many oranges: '` e imprime el resultado.

# Info

En `JavaScript` las funciones se declaran usando la __palabra clave__ `function`, y sus argumentos se escriben entre paréntesis después del nombre de la función:

```javascript
function maSum (a, b) {
  return a + b
}

const ans = maSum(5, 4)
```

En el ejemplo definimos una _función_ llamada __maSum__ que recibe 2 argumentos, los suma y retorna el resultado. Así, __ans__ debería contener un valor de `9`.

## Una manera alternativa

```javascript
const maSum = (a, b) => {
  return a + b
}

maSum(5, 4)
```

En este ejemplo la función hace exactamente lo mismo que en la anterior, pero en este caso se declara como una __variable__ que apunta a una __arrow function__, o __función flecha__, por ahora no te preocupes por los nombres, solo ten claro que ambas declaraciones son válidas y la función cumple el mismo objetivo, pero es bueno entender que _JavaScript_ es programación _funcional_!, tus funciones también son variables!.