# El Reto

Declara un _arreglo_ con los siguientes elementos: `'apple'`, `'banana'`, `'durian'`, `'pineapple'`, luego imprime el tercer elemento, luego filtra `'durian'` del arreglo y asigna este arreglo filtrado a una nueva variable, recorre este nuevo arreglo, agregando una `'s'` al final de cada palabra para hacerla plural, finalmente, imprime el arreglo con las palabras pluralizadas.

# Info

En `JavaScript` un arreglo o _array_ es una __lista__ de valores, se declaran como cualquier variable, con elementos entre paréntesis cuadrados (`[]`), y como los `Strings`, contienen gran cantidad de métodos preimplementados, útiles para su procesamiento:

```javascript
const numArray = [1, 2, 3, 4, 5, 6]
const three = numArray[2]
const elementCount = numArray.length

const filterOdd => (elem) {
  if (elem % 2 == 0) {
    return true
  }
  return false
}

const evenArray = numArray.filter(filterOdd)
```

En el ejemplo anterior se declara un array de números numArray (nótese que un array puede tener contenido mixto), se asigna a la variable __three__ el tercer elemento del arreglo (los índices para acceder a elementos del arreglo empizan en `0`), y se asigna el número de elementos del arreglo a la variable __elementCount__, luego se define una función __filterOdd__ que recibe un elemento y determina si es _par_ o _impar_, retorna `true` si es _par_ y `false` de lo contrario.

## Callbacks

En este punto es buento entender un concepto importante de `JavaScript`, el concepto de __Callback__, como vimos anteriormente, una función en `JavaScript` es también una __variable__, por consecuencia, existen funciones que reciben otra función como alguno de sus parámetros, para luego ejecutarla con determinados argumentos, la función que se envía como parámetro se conoce como función __Callback__.

En el ejemplo anterior, __filter__ es una función que recibe otra función como parámetro, esta función se ejecuta sobre cada elemento del arreglo para determinar si debe ser filtrado o no, si la función retorna `true`, el elemento permanece en el arreglo, de lo contrario se filtra, finalmente __filter__ retorna el arreglo filtrado. Nuestra función callback __filterOdd__ y __evenArray__ debería contener entonces sólo números pares.